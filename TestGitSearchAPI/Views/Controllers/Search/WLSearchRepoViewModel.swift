//
//  WLSearchRepoControllerViewModel.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import SwiftyJSON

class WLSearchRepoViewModel {
    
    //MARK: Properties
    private(set) var indexLastVisibleCell: Int = -1
    private(set) var position: Int = 0
    private(set) var totalCounts: Int = 0
    private(set) var runSearchRequests: Array<Bool> = [false, false]
    
    private(set) var listModels: Array<WLRepoModel> = [WLRepoModel]()
    
    var resultBlock: CompletionHandlerViewModel?
    private(set) var term: String?
    
    private var searchBlocks: Array<DispatchWorkItem> = [DispatchWorkItem]()
    private let searchGroup = DispatchGroup()
    
    private let queueSearchResult = DispatchQueue(label: "safe-queueSearchResult", qos: .default, attributes: .concurrent)
    private var searchResultState: Array<Array<WLResultEnum>> = [[], []]
    
    private var callbackResult: CompletionHandler!
    
    //MARK: LifeCycle
    init() {
        configureSearchCallback()
    }
    
    func updateTerm(_ value: String?) {
        guard let newValue = value?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else { return }
        guard newValue != self.term, newValue.count > 2 else { return }
        
        // 2.
        self.term = newValue
        self.indexLastVisibleCell = -1
        self.position = 0
        self.totalCounts = 0
        self.listModels.removeAll()
        self.searchBlocks.forEach({ $0.cancel() })
        self.searchBlocks.removeAll()
        
        fetchData()
    }
    
    func updateIndexLastVisibleCell(at index: Int) {
        self.indexLastVisibleCell = self.indexLastVisibleCell < index ? index : self.indexLastVisibleCell
    }
    
    //MARK: Methods
    func stopSearch() {
        ApiManager.sharedInstance.stopAllSearchTasks()
    }
    
    func fetchData() {
        if let _ = self.term,
            self.indexLastVisibleCell >= self.listModels.count - 10 &&
            self.position == self.listModels.count &&
            self.runSearchRequests[0] == false && self.runSearchRequests[1] == false
        {
            debugPrint("Search value", self.position," last index", indexLastVisibleCell)
            
            if ApiManager.sharedInstance.statusConnection {
                fetchRepo(at: self.position)
            } else if let _resultBlock = self.resultBlock {
                _resultBlock(WLResultEnum.success(data: nil))
            }
        }
    }
    
    func fetchRepo(at position: Int = 0) {
        self.position = position
        
        self.runSearchRequests = self.runSearchRequests.map({ _ in return true })
        
        // 2.
        for index in 0..<self.runSearchRequests.count {
            self.searchGroup.enter()
            
            let block = DispatchWorkItem(qos: DispatchQoS.default, flags: DispatchWorkItemFlags.inheritQoS) { [weak self]() in
                guard let weakSelf = self else { return }
                ApiManager.sharedInstance.searchBy(term: weakSelf.term, page: (weakSelf.position + index * Constants.API.offset) / Constants.API.offset, completionHandler: weakSelf.callbackResult)
            }
            self.searchBlocks.append(block)
            block.perform()
        }
    
        // 3.
        self.searchGroup.notify(queue: DispatchQueue.main) { [weak self]() in
            guard let weakSelf = self else { return }
            weakSelf.handleSearchResult()
        }
    }
    
    private func configureSearchCallback() {
        self.callbackResult = { [weak self](data, error) in
            guard let weakSelf = self else { return }
            defer {
                weakSelf.searchGroup.leave()
            }
            
            let index = (data as! JSON)[Constants.Keys.page].intValue % 2
            
            guard error == nil else {
                weakSelf.queueSearchResult.async(flags: .barrier) {
                    weakSelf.searchResultState[index].append(WLResultEnum.failure(error: error))
                }
                return
            }
            
            let dataJSON = data as! JSON
            let list = dataJSON[Constants.Keys.items].arrayValue.map({ WLRepoModel($0)})
            weakSelf.totalCounts = dataJSON[Constants.Keys.totalCount].intValue
            
            weakSelf.queueSearchResult.sync(flags: .barrier) {
                weakSelf.searchResultState[index].append(WLResultEnum.success(data: list))
            }
        }
    }
    
    private func handleSearchResult() {
        guard let _resultBlock = self.resultBlock else { return }
        
        defer {
            self.runSearchRequests = self.runSearchRequests.map({ _ in return false })
            for (index, _ ) in self.searchResultState.enumerated() {
                self.searchResultState[index].removeAll()
            }
        }
        
        for (index, value) in self.searchResultState.enumerated() {
            
            if let _result = value.first {
                switch _result {
                case .failure(let error):
                    if index == 0 {
                        _resultBlock(WLResultEnum.failure(error: error))
                        return
                    }
                    break
                case .success(let data):
                    let list = data as! Array<WLRepoModel>
                    self.listModels.append(contentsOf: list)
                    self.position += list.count
                    list.forEach({ DataManager.sharedInstance.updateRepo($0)})
                    break
                }
            }
        }
        
        _resultBlock(WLResultEnum.success(data: nil))
    }
}







