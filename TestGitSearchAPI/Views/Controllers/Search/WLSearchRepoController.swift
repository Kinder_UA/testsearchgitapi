//
//  WLSearchRepoController.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class WLSearchRepoController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    //MARK: Properties
    let searchRepoViewModel = WLSearchRepoViewModel()
    private var searchRepoCoordinator: WLSearchRepoCoordinator!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareParameters()
    }
    
    //MARK: Methods
    private func prepareParameters() {
        self.searchRepoCoordinator = WLSearchRepoCoordinator(self.navigationController!)
        
        configureTable()
    }
    
    private func configureTable() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        registerCell(WLRepoCell.kIdentifier)
        
        self.tableView.estimatedRowHeight = 72.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.tableFooterView = UIView()
        
        self.searchBar.delegate = self
        DataManager.sharedInstance.fetchedResultController.delegate = self
        
        configureCallback()
        
        DataManager.sharedInstance.showMainStorage()
    }
    
    private func registerCell(_ identifier: String) {
        let nib = UINib.init(nibName: identifier, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: identifier)
    }
    
    private func fetchStoredData() {
        do {
            DataManager.sharedInstance.clearCache()
            try DataManager.sharedInstance.fetchedResultController.performFetch()
        } catch {
            debugPrint(error)
        }
    }
    
    private func configureCallback() {
        self.searchRepoViewModel.resultBlock = { [weak self](_ result: WLResultEnum) in
            guard let weakSelf = self else { return }

            switch result {
            case .failure(let error):
                if let _error = error, _error.code != NSURLErrorCancelled {
                    ApiManager.handleErrorMessage(error: _error)
                }
            case .success(_):
                DataManager.sharedInstance.updatePredicate(term: weakSelf.searchRepoViewModel.term)
                weakSelf.fetchStoredData()
                DataManager.sharedInstance.showMainStorage()
                weakSelf.tableView?.reloadData()
            }
        }
    }
    
    //MARK: Actions
    @IBAction private func stopSearch(_ sender: AnyObject) {
        self.searchRepoViewModel.stopSearch()
    }
}

//MARK:- UITableViewDelegate & UITableViewDataSource
extension WLSearchRepoController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataManager.sharedInstance.fetchedResultController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.sharedInstance.fetchedResultController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureCell(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint("Selected cell at: \(indexPath.section),\(indexPath.row)")
        
        guard let sections = DataManager.sharedInstance.fetchedResultController.sections,
            let itemsInSection = sections[indexPath.section].objects as? [WLRepoEntity]
            else { return }
        
        let repo = itemsInSection[indexPath.row]
        self.searchRepoCoordinator.routeToWeb(pageUrl: repo.pageUrl)
    }
    
    private func configureCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: WLRepoCell.kIdentifier) as! WLRepoCell
        cell.selectionStyle = .none
        
        guard let sections = DataManager.sharedInstance.fetchedResultController.sections else {
            return cell
        }
        
        let section = sections[indexPath.section]
        guard let itemsInSection = section.objects as? [WLRepoEntity] else {
            return cell
        }
        let repo = itemsInSection[indexPath.row]
        
        cell.update(WLRepoCellViewModel.init(repo))
        
        self.searchRepoViewModel.updateIndexLastVisibleCell(at: indexPath.row)
        
        return cell
    }
}

//MARK:- NSFetchedResultsControllerDelegate
extension WLSearchRepoController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .move:
            self.tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet.init(integer: sectionIndex) , with: .automatic)
        case .delete:
            self.tableView.deleteSections(IndexSet.init(integer: sectionIndex) , with: .automatic)
        case .move, .update:
            self.tableView.reloadSections(IndexSet.init(integer: sectionIndex) , with: .automatic)
        }
    }
}

//MARK:- UIScrollViewDelegate
extension WLSearchRepoController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        debugPrint("scrollViewDidEndDecelerating", self.searchRepoViewModel.indexLastVisibleCell)
        searchAfterStopScroll()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            debugPrint("scrollViewDidEndDragging", self.searchRepoViewModel.indexLastVisibleCell)
            searchAfterStopScroll()
        }
    }
    
    func searchAfterStopScroll() {
        self.searchRepoViewModel.fetchData()
    }
}

//MARK:- UIScrollViewDelegate
extension WLSearchRepoController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchRepoViewModel.updateTerm(searchBar.text)
    }
}
