//
//  WLSearchRepoControllerCoordinator.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit

class WLSearchRepoCoordinator {
    
    //MARK: Properties
    fileprivate var navController: UINavigationController!
    
    //MARK: LifeCycle
    init(_ navController: UINavigationController) {
        self.navController = navController
    }
    
    //MARK: Routes
    func routeToWeb(pageUrl: String?) {
        guard let _pageUrl = pageUrl else { return }
        
        let vc = HelperManager.getController(WLWebController.kIdentifier, forStoryboardName: Constants.Storyboard.web) as! WLWebController
        vc.webViewModel = WLWebViewModel(_pageUrl)
        
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        self.navController.present(vc, animated: true, completion: nil)
    }
}
