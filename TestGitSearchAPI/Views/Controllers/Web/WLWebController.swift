//
//  WLWebController.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import MBProgressHUD

class WLWebController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet private weak var vwContent: UIView!
    @IBOutlet private weak var btnBack: UIButton!
    
    //MARK: Properties
    var webViewModel: WLWebViewModel!
    private var wbView: WKWebView?
    private var canGoBack: Bool = false {
        didSet {
            self.btnBack.isHidden = !canGoBack
        }
    }
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareParameters()
    }
    
    deinit {
        self.wbView?.stopLoading()
        self.wbView?.removeObserver(self, forKeyPath: "loading")
    }
    
    //MARK: Methods
    private func prepareParameters() {
        self.vwContent.layer.cornerRadius = 10.0
        self.vwContent.layer.masksToBounds = true
        
        dismissOnTapEmptySpace()
        self.vwContent.ignoreTapEmptySpace()
        
        prepareWebView()
    }
    
    private func prepareWebView() {
        self.wbView = WKWebView.init(frame: self.vwContent.bounds)
        
        self.wbView?.navigationDelegate = self
        self.wbView?.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        
        self.vwContent.addSubview(self.wbView!)
        self.vwContent.addConstraintWithFormat(format: "H:|-0.0-[v0]-0.0-|", views: self.wbView!)
        self.vwContent.addConstraintWithFormat(format: "V:|-[v0]-|", views: self.wbView!)
        self.vwContent.layoutIfNeeded()
        
        self.wbView?.load(URLRequest.init(url: webViewModel.pathUrl))
    }
    
    //MARK: observeValue
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "loading") {
            self.canGoBack = self.wbView?.canGoBack ?? false
        }
    }
    
    //MARK:- Actions
    @IBAction private func actionBack(_ sender: AnyObject) {
        if self.canGoBack {
            self.wbView?.goBack()
        }
    }
}

//MARK: - WKNavigationDelegate
extension WLWebController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        debugPrint("Error web: \(error.localizedDescription)")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(WKNavigationActionPolicy.allow)
    }
}
