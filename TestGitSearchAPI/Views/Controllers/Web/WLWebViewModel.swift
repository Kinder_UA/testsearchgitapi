//
//  WLWebControllerViewModel.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

class WLWebViewModel {
    
    //MARK: Properties
    var pathUrl: URL!
    
    //MARK: LifeCycle
    init(_ path: String) {
        self.pathUrl = URL.init(string: path.pathUrlCoding())
    }
    
    //MARK: Methods
    
}
