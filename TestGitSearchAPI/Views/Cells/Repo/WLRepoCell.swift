//
//  WLRepoCell.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit

class WLRepoCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblDescription: UILabel!
    @IBOutlet private weak var lblRating: UILabel!
    
    //MARK: LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        [self.lblTitle, self.lblRating, self.lblDescription].forEach({ $0?.text = "" })
    }
    
    func update(_ data: WLRepoCellViewModel) {
        self.lblTitle.text = data.title()
        self.lblRating.text = data.rating()
        self.lblDescription.text = data.description()
    }
}



