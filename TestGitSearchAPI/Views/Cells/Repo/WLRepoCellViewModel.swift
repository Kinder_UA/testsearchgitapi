//
//  WLRepoCellViewModel.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

struct WLRepoCellViewModel {
    
    //MARK: Properties
    private var repoModel: WLRepoEntity!
    
    //MARK: LifeCycle
    init(_ data: WLRepoEntity) {
        self.repoModel = data
    }
    
    //MARK: Methods
    func title() -> String {
        return cutString(self.repoModel.title ?? "", maxLength: Constants.maxLenght)
    }
    
    func rating() -> String {
        return "Rating: \(self.repoModel.rating)"
    }
    
    func description() -> String {
        return cutString(self.repoModel.desc ?? "", maxLength: Constants.maxLenght)
    }
    
    private func cutString(_ strInput: String, maxLength: Int) -> String {
        let strOutput = strInput.trimmingCharacters(in: .whitespacesAndNewlines)
        guard strOutput.count > maxLength else {
            return strOutput
        }
        
        let index = strOutput.index(strOutput.startIndex, offsetBy: maxLength)
        return String(strOutput[..<index])
    }
    
}
