//
//  DataManager.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import CoreData

class DataManager {
    
    private let databaseName: String = "TestGitSearchAPI"
    private let casheName: String = "casheGitSearchAPI"
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: self.databaseName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var fetchedResultController: NSFetchedResultsController<WLRepoEntity> = {
        
        let frc = NSFetchedResultsController(fetchRequest: self.fetchRequest,
                                             managedObjectContext: self.persistentContainer.viewContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: self.casheName)
        frc.fetchRequest.fetchBatchSize = Constants.API.offset * 2
        
        return frc
    }()
    
    private lazy var fetchRequest: NSFetchRequest<WLRepoEntity> = {
        let fetchRequest = NSFetchRequest<WLRepoEntity>(entityName: WLRepoEntity.entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "rating", ascending: false), NSSortDescriptor(key: "title", ascending: true)]
        
        return fetchRequest
    }()
    
    //MARK: Instance
    static let sharedInstance = DataManager()
    
    private init() {
    }
}

//MARK:- Methods
extension DataManager {
    
    func clearCache() {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: self.casheName)
    }
    
    func updatePredicate(term: String?) {
        guard let _term = term else { return }
        
        let aRequest = self.fetchedResultController.fetchRequest
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@", _term)
        aRequest.predicate = predicate
    }
    
    func createRepoObject(_ repo: WLRepoModel) {
        let repoEntity = self.persistentContainer.viewContext.insert(entity: WLRepoEntity.self)
        
        saveEntity(repoEntity, for: repo)
    }
    
    func updateRepo(_ repo: WLRepoModel) {
        guard let repoEntity = self.persistentContainer.viewContext.fetchAll(entity: WLRepoEntity.self, fetchConfiguration: { (_fetchRequest) in
            _fetchRequest.predicate = NSPredicate(format: "id == %i", repo.id)
        })?.first as? WLRepoEntity else {
            
            createRepoObject(repo)
            return
        }
        
        saveEntity(repoEntity, for: repo)
    }
    
    private func saveEntity(_ repoEntity: WLRepoEntity, for repo: WLRepoModel) {
        repoEntity.title = repo.title
        repoEntity.desc = repo.desc
        repoEntity.pageUrl = repo.pageUrl
        repoEntity.rating = Int32(repo.rating)
        repoEntity.id = Int32(repo.id)
        
        saveContext()
    }
    
    // MARK: Core Data Saving support
    func saveContext () {
        let context = self.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                debugPrint("Error \((error as NSError).userInfo)")
            }
        }
    }
    
    func showMainStorage() {
        var list: Array<WLRepoEntity>!
        do {
            list = try self.persistentContainer.viewContext.fetch(self.fetchRequest)
        } catch {
            debugPrint("Storage reading error!")
        }
        
        debugPrint("Count: \(list.count)")
        for repo in list {
            debugPrint("Repo: id: \(repo.id) name: \(repo.title)")
        }
    }
}

//MARK:- NSManagedObject
extension NSManagedObject {
    class var entityName: String {
        let components = NSStringFromClass(self)
        
        return components
    }
    
    class func fetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
        
        return request
    }
    
    class func fetchedRequestWith(key: String, asceding: Bool = true) -> NSFetchRequest<NSFetchRequestResult> {
        let request = fetchRequest()
        request.sortDescriptors = [NSSortDescriptor.init(key: key, ascending: asceding)]
        
        return request
    }
}

//MARK:- NSManagedObjectContext
extension NSManagedObjectContext {
    func insert<T: NSManagedObject>(entity: T.Type) -> T {
        let entityName = entity.entityName
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: self) as! T
    }
    
    func fetchAll<T: NSManagedObject>(entity: T.Type, fetchConfiguration: ((NSFetchRequest<NSManagedObject>) -> ())?) -> [NSManagedObject]? {
        let dataFetchRequest = NSFetchRequest<NSManagedObject>.init(entityName: entity.entityName)
        
        fetchConfiguration?(dataFetchRequest)
        
        var result = [NSManagedObject]()
        do {
            result = try self.fetch(dataFetchRequest)
        } catch {
            debugPrint("Error")
        }
        
        return result
    }
}

