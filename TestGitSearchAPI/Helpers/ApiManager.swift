//
//  ApiManager.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol WLProtocolApi {
    func searchBy(term: String?, page: Int, completionHandler: @escaping CompletionHandler)
}

typealias CompletionHandler = (_ data: Any?, _ error: WLApiError?) -> Void

//MARK:- ApiManager
class ApiManager {
    struct Routes {
        static let searchByTerm: String = "search/repositories"
    }
    
    //MARK: Properties
    let reachabilityManager = Reachability()!
    var statusConnection: Bool {
        get {
            let _status = ApiManager.sharedInstance.reachabilityManager.connection == .none ? false : true
            return _status
        }
    }
    
    var sessionManager: SessionManager!
    var requests: Array<Array<Request>> = [[Request](), [Request]()]
    
    //MARK: Instance
    static let sharedInstance = ApiManager()
    
    private init() {
        
        self.sessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
    }
}

extension ApiManager {
    //MARK: Requests
    func makeHTTPRequest(method: HTTPMethod, path: String, parameters: Dictionary<String, Any>?, encodingType: ParameterEncoding = URLEncoding.default, completionHandler: @escaping CompletionHandler) -> DataRequest {
        
        let task = self.sessionManager.request(path.pathUrlCoding(),
                                               method: method,
                                               parameters: parameters,
                                               encoding: encodingType)
            .validate()
            .responseJSON(queue: DispatchQueue.global(), completionHandler: { (response) in
                
                let page = parameters![Constants.Keys.page] as! Int
                let pageJSON = JSON([Constants.Keys.page: page - 1])
                
                if response.result.isFailure {
                    ApiManager.sharedInstance.handleErrorData(response: response, completionHandler: completionHandler, pageInfo: pageJSON)
                    return
                } else {
                    var dataResult = JSON(response.result.value!)
                    debugPrint(dataResult)
                    
                    try? dataResult.merge(with: pageJSON)
                    completionHandler(dataResult, nil)
                }
        })
        //task.resume()
        return task
    }
    
    //MARK: Helpers
    private func handleErrorData(response: DataResponse<Any>, completionHandler: CompletionHandler?, pageInfo: JSON? = nil) {
        
        #if DEBUG
        debugPrint("Error in translation request: \(String(describing: response.result.error?.localizedDescription))")
        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            debugPrint("Data: \(utf8Text)")
        }
        #endif
        
        let dataResult = try? JSON(data: response.data!)
        let statusCode = response.response?.statusCode
        
        #if DEBUG
        debugPrint(dataResult ?? "nil")
        debugPrint(statusCode ?? "nil")
        #endif
        
        if let handler = completionHandler {
            let error = WLApiError(with: response.result.error, data: dataResult, statusCode: statusCode)
            handler(pageInfo, error)
        }
    }
    
    class func handleErrorMessage(data: Any? = nil, error: WLApiError?) {
        
        guard let _error = error else { return }
        
        var strErrorMessage: String? = nil
        strErrorMessage = _error.message ?? _error.localizedDescription
        
        debugPrint("Error: \(strErrorMessage ?? "___errorMessage___")")
        
        HelperManager.showDBAlert(title: Constants.Keys.errorTitle, message: strErrorMessage)
    }
    
    func stopAllSearchTasks() {
        for index in 0..<self.requests.count {
            self.requests[index].forEach({ $0.cancel() })
            self.requests[index].removeAll()
        }
    }
}

//MARK:- Methods
extension ApiManager: WLProtocolApi {
    func searchBy(term: String?, page: Int, completionHandler: @escaping CompletionHandler) {
        let urlPath = Constants.API.baseUrl + Routes.searchByTerm
        
        var parameters: Dictionary<String, Any> = [Constants.Keys.order: Constants.API.orderValue,
                                                   Constants.Keys.page: page + 1,
                                                   Constants.Keys.sort: Constants.API.sortValue,
                                                   Constants.Keys.offset: Constants.API.offset]
        
        if let _term = term {
            parameters[Constants.Keys.term] = _term
        }
        
        // 2.
        let index = page % 2
        self.requests[index].forEach({ $0.cancel() })
        self.requests[index].removeAll()
        
        // 3.
        let task = self.makeHTTPRequest(method: .get, path: urlPath, parameters: parameters, completionHandler: completionHandler)
        self.requests[index].append(task)
    }
}







