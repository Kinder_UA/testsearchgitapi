//
//  Constants.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let maxLenght: Int = 30
    
    struct API {
        static let baseUrl: String = "https://api.github.com/"
        static let offset: Int = 15
        static let sortValue: String = "stars"
        static let orderValue: String = "desc"
    }
    
    struct Storyboard {
        static let main: String = "Main"
        static let web: String = "Web"
    }
    
    struct Colors {
        static let mainBlue: UIColor = #colorLiteral(red: 0, green: 0.5921568627, blue: 1, alpha: 1)
    }
    
    struct Keys {
        static let errorTitle: String = "Error"
        static let message: String = "message"
        
        static let term: String = "q"
        static let sort: String = "sort"
        static let order: String = "order"
        static let page: String = "page"
        static let offset: String = "per_page"
        
        static let id: String = "id"
        static let name: String = "name"
        static let htmlUrl: String = "html_url"
        static let description: String = "descriptin"
        static let totalCount: String = "total_count"
        static let items: String = "items"
        static let rating: String = "stargazers_count"
    }
}

