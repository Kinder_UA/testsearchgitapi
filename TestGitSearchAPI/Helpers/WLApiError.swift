//
//  WLApiError.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WLApiError {
    var localizedDescription: String
    var code: Int? = nil
    var message: String? = nil
    var userInfo: [String: Any]? = nil
    
    init(with error: Error?, data: JSON?, statusCode: Int? = nil) {
        self.localizedDescription = error?.localizedDescription ?? "Unknow error"
        
        if let _error = error as NSError? {
            self.userInfo = _error.userInfo
            self.code = _error.code
        }
        
        // 2.
        if let _dataDictionary = data?.dictionaryObject {
            self.userInfo = _dataDictionary
            if let _data = data, let _message = _data[Constants.Keys.message].string {
                self.message = _message
            }
        }
    }
}
