//
//  AppExtensions.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit

//MARK:- NSObject
extension NSObject {
    
    open class var kIdentifier: String  {
        get {
            return String(describing: self)
        }
    }
}

//MARK:- String
extension String {
    func pathUrlCoding() -> String {
        let result = self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        //debugPrint("Encoding: ", self)
        return result ?? ""
    }
}

//MARK:- UIViewController
extension UIViewController {

    func dismissOnTapEmptySpace() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- UIView
extension UIView {
    func ignoreTapEmptySpace() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.actionIgnore))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func actionIgnore() {
    }
    
    func addConstraintWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
