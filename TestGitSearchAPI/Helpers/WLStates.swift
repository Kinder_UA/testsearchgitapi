//
//  WLStates.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

enum WLResultEnum {
    case success(data: Any?)
    case failure(error: WLApiError?)
}

typealias CompletionHandlerViewModel = (_ result: WLResultEnum) -> Void
