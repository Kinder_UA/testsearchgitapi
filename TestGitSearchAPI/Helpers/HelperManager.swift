//
//  HelperManager.swift
//  TestGitSearchAPI
//
//  Created by User on 05.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit

class HelperManager {
    //MARK: Properties
    private var alertViewDB: DBAlertController?
    
    //MARK: Instance
    static let sharedInstance = HelperManager()

    private init() {
    }
}

extension HelperManager {
    
    //MARK: Controller
    class func getController(_ vcIdentifier: String, forStoryboardName: String) -> UIViewController {
        let storyboard = UIStoryboard(name: forStoryboardName, bundle: nil)
        let vcController = storyboard.instantiateViewController(withIdentifier: vcIdentifier)
        
        return vcController
    }
    
    //MARK: Open web page in safari
    class func openWebPage(url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: Alert
    class func showDBAlert(title: String?, message: String?) {
        
        DispatchQueue.main.async (flags: .barrier) {
            
            if HelperManager.sharedInstance.alertViewDB != nil {
                return
            }
            
            HelperManager.sharedInstance.alertViewDB = DBAlertController(title: title, message: message, preferredStyle: .alert)
            HelperManager.sharedInstance.alertViewDB?.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                HelperManager.sharedInstance.alertViewDB = nil
            }))
            HelperManager.sharedInstance.alertViewDB?.show()
        }
    }
}
