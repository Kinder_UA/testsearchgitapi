//
//  WLRepoModel.swift
//  TestGitSearchAPI
//
//  Created by User on 08.07.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WLRepoModel {
    var id: Int
    var title: String
    var desc: String
    var pageUrl: String
    var rating: Int
    
    init(_ data: JSON) {
        self.id = data[Constants.Keys.id].intValue
        self.rating = data[Constants.Keys.rating].intValue
        self.title = data[Constants.Keys.name].stringValue
        self.desc = data[Constants.Keys.description].stringValue
        self.pageUrl = data[Constants.Keys.htmlUrl].stringValue
    }
}
