//
//  WLRepoEntity+CoreDataProperties.swift
//  
//
//  Created by User on 08.07.2018.
//
//

import Foundation
import CoreData


extension WLRepoEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WLRepoEntity> {
        return NSFetchRequest<WLRepoEntity>(entityName: "WLRepoEntity")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: Int32
    @NSManaged public var pageUrl: String?
    @NSManaged public var rating: Int32
    @NSManaged public var title: String?

}
